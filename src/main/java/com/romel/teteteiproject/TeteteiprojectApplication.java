package com.romel.teteteiproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeteteiprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeteteiprojectApplication.class, args);
    }

}
