package com.romel.teteteiproject.security;

import com.romel.teteteiproject.service.ProjectUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Slf4j
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final ProjectUserDetailsService projectUserDetailsService;

    public SecurityConfig(ProjectUserDetailsService projectUserDetailsService) {
        this.projectUserDetailsService = projectUserDetailsService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /*  Passa a segurança em todos os endpoints */
        http.csrf().disable()
//                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
//                .and()
                .authorizeRequests()
                .anyRequest()
                .authenticated()
                .and()
                /*  Define que o tipo de autenticação é httpbasic   */
                .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
//        log.info("Testing password encoder cliente {}",passwordEncoder.encode("123"));
//        log.info("Testing password encoder ativos {}",passwordEncoder.encode("ativos2021"));

        /*  Autenticação dentro do código (fora do Banco de Dados)  */
//        auth.inMemoryAuthentication()
//                .withUser("cliente")
//                .password(passwordEncoder.encode("123"))
//                .roles("USER")
//                .and()
//                .withUser("ativos")
//                .password(passwordEncoder.encode("ativos2021"))
//                .roles("USER", "ADMIN");

        /*  Autenticação pelo Banco de Dados    */
        auth.userDetailsService(projectUserDetailsService)
        .passwordEncoder(passwordEncoder);
    }
}
