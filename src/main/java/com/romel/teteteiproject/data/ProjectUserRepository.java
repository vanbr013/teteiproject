package com.romel.teteteiproject.data;

import com.romel.teteteiproject.model.entity.ProjectUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface ProjectUserRepository extends JpaRepository<ProjectUserEntity, String> {

    ProjectUserEntity findByUsername(String username);
}
