package com.romel.teteteiproject.data;

import com.romel.teteteiproject.model.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, String> {

    List<ProductEntity> findByName(String name);
    ProductEntity findByCode(String code);
}
