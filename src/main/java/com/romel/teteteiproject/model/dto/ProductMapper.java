package com.romel.teteteiproject.model.dto;

import com.romel.teteteiproject.model.entity.ProductEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class ProductMapper {
    public static final ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    public abstract ProductEntity toEntity(ProductDto productDto);
    public abstract ProductDto toDto(ProductEntity productEntity);
}
