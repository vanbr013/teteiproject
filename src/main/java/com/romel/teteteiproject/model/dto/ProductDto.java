package com.romel.teteteiproject.model.dto;

import lombok.Data;

@Data
public class ProductDto {
    private String name;
    private String code;
    private boolean isentoDeImposto;
}
