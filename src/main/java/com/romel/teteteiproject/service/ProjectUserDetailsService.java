package com.romel.teteteiproject.service;

import com.romel.teteteiproject.data.ProjectUserRepository;
import com.romel.teteteiproject.model.entity.ProjectUserEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectUserDetailsService implements UserDetailsService {

    private final ProjectUserRepository projectUserRepository;

    public ProjectUserDetailsService(ProjectUserRepository projectUserRepository) {
        this.projectUserRepository = projectUserRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ProjectUserEntity userDetailResponse = Optional.ofNullable(projectUserRepository.findByUsername(username))
                .orElseThrow(() -> new UsernameNotFoundException("User not found!"));

        return userDetailResponse;
    }

    public List<ProjectUserEntity> listAllUsers(){
        return projectUserRepository.findAll();
    }

    public ProjectUserEntity saveUser(ProjectUserEntity userEntity){
        PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        userEntity.setPassword(passwordEncoder.encode(userEntity.getPassword()));
        return projectUserRepository.save(userEntity);
    }

    public void deleteUser(String userEntityId){
        projectUserRepository.deleteById(userEntityId);
    }
}
