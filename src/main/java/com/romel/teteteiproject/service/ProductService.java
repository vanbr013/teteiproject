package com.romel.teteteiproject.service;

import com.romel.teteteiproject.data.ProductRepository;
import com.romel.teteteiproject.model.dto.ProductDto;
import com.romel.teteteiproject.model.dto.ProductMapper;
import com.romel.teteteiproject.model.entity.ProductEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<ProductEntity> listAll() {
        return productRepository.findAll();
    }

    public ProductDto findByCode(String code){
        return ProductMapper.INSTANCE.toDto(productRepository.findByCode(code));
    }

    public ProductEntity save(ProductDto productDto){
        ProductEntity productEntity = new ProductEntity();
        productEntity.setCode(ProductMapper.INSTANCE.toEntity(productDto).getCode());
        productEntity.setName(ProductMapper.INSTANCE.toEntity(productDto).getName());
        productEntity.setIsentoDeImposto(ProductMapper.INSTANCE.toEntity(productDto).isIsentoDeImposto());
        return productRepository.save(productEntity);
    }

    public void delete(String productEntityId){
        productRepository.deleteById(productEntityId);
    }
}
