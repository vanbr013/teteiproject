package com.romel.teteteiproject.rest;

import com.romel.teteteiproject.data.ProjectUserRepository;
import com.romel.teteteiproject.model.entity.ProductEntity;
import com.romel.teteteiproject.model.entity.ProjectUserEntity;
import com.romel.teteteiproject.service.ProductService;
import com.romel.teteteiproject.service.ProjectUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("manage")
public class ProjectUserRest {

    @Autowired
    private ProjectUserRepository projectUserRepository;

    @Autowired
    private ProjectUserDetailsService projectUserDetailsService;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(path = "/list")
    @ResponseStatus(HttpStatus.OK)
    public List<ProjectUserEntity> listAll() {
        return projectUserDetailsService.listAllUsers();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProjectUserEntity save(@RequestBody ProjectUserEntity entity) {
        return projectUserDetailsService.saveUser(entity);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(path = "/{Id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable(name = "Id") String UserEntityId){
        projectUserDetailsService.deleteUser(UserEntityId);
    }
}
