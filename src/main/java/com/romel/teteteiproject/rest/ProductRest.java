package com.romel.teteteiproject.rest;

import com.romel.teteteiproject.model.dto.ProductDto;
import com.romel.teteteiproject.model.entity.ProductEntity;
import com.romel.teteteiproject.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("product")
public class ProductRest {

    private final ProductService productService;

    public ProductRest(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(path = "/list")
    @ResponseStatus(HttpStatus.OK)
    public List<ProductEntity> listAll() {
        return productService.listAll();
    }

    @GetMapping(path = "/{Code}")
    public ProductDto findByCode(@PathVariable(name = "Code") String code){
        return productService.findByCode(code);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProductEntity save(@RequestBody ProductDto productDto) {
        return productService.save(productDto);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(path = "/{Id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable(name = "Id") String productEntityId){
        productService.delete(productEntityId);
    }
}
